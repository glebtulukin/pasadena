variable "region" {
 default = "us-east-1"
}
variable "availabilityZone" {
        default = "us-east-1a"
}

variable "security_group_id" {
        default = "sg-08830c74ceb3f2a82"
}

variable "subnet_id" {
        default = "subnet-0518e09add3f55a28"
}

