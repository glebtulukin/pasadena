#!/usr/bin/env ruby

require('date')
require('open-uri')
require('json')

api_key = ENV["MY_API_KEY"]
if api_key.nil?
    puts "The environment variable MY_API_KEY is not defined"
    exit(1)
end    

d = DateTime.now() #initialize with current date/time
past_week_times=(1..7).map { |i| d = d.prev_day; d.to_time.to_i } #go back 7 days and convert to epoch

puts "past 7 days with min temp for the day"
past_week_lows=[]
past_week_times.each { |t|
    request_url = "https://api.darksky.net/forecast/#{api_key}/34.1477849,-118.1445155,#{t}?exclude=currently,hourly,flags"
    data_obj = JSON.load(open(request_url))["daily"]["data"][0]
    past_week_lows << { :temp_min => data_obj["temperatureMin"].to_f, :temp_min_date => Time.at( t ).to_date }
}

puts past_week_lows

week_min = past_week_lows.min_by { |l| l[:temp_min] }
puts "\nthe past week coldest temperature/date was:"
puts week_min

puts "\ngenerating HTML..."
html = <<HTML
<html>
    <body>
        <h1>Pasadena coldest day for the past week was</h1>
        #{week_min[:temp_min_date].strftime('%Y-%m-%d')} with temperature #{week_min[:temp_min]}
    </body>
</html>
HTML

puts html

File.open('generated/index.html', 'w') { |f|
    f.puts html
}

puts "done"

