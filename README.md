# Pasadena Weather

## Ruby script to generate static index.html page - generate_html.rb
The are comments in the Ruby script -- it checks that the API key variable is set, then it creates an array with timestamps and queries the Darksky API, then outputs a generated HTML page to generated/index.html.  

## Assumptions
- VPC is precreated (that's beyond this assignment)
- the passadina.pem is precreated
- you have Darksky API key that you will export with export MY_API_KEY=xxxxxxx
- you have `~/.aws` properly configured with AWS credentials

## Security Group with correct CIDRs as requested in the requirements
Below is the aws cli command to describe the security group. This is to show that SG has outbound rules to pull required packages or source code from GitLab, accessible from the Internet on port 80 and on port 22 only from listed IPs.

```
glebtulukin@DESKTOP-1BJSHAN:/mnt/c/work/sandbox/07-23-pasadena/pasadena$ aws ec2 describe-security-groups --group-id sg-08830c74ceb3f2a82
{
    "SecurityGroups": [
        {
            "IpPermissionsEgress": [
                {
                    "IpProtocol": "-1",
                    "PrefixListIds": [],
                    "IpRanges": [
                        {
                            "CidrIp": "0.0.0.0/0"
                        }
                    ],
                    "UserIdGroupPairs": [],
                    "Ipv6Ranges": []
                }
            ],
            "Description": "new SG for the Irving VPC",
            "IpPermissions": [
                {
                    "PrefixListIds": [],
                    "FromPort": 80,
                    "IpRanges": [
                        {
                            "CidrIp": "0.0.0.0/0"
                        }
                    ],
                    "ToPort": 80,
                    "IpProtocol": "tcp",
                    "UserIdGroupPairs": [],
                    "Ipv6Ranges": [
                        {
                            "CidrIpv6": "::/0"
                        }
                    ]
                },
                {
                    "PrefixListIds": [],
                    "FromPort": 22,
                    "IpRanges": [
                        {
                            "Description": "my Windows laptop",
                            "CidrIp": "71.114.22.117/32"
                        },
                        {
                            "Description": "as requested in the assignment",
                            "CidrIp": "172.251.52.5/32"
                        },
                        {
                            "Description": "my Ubuntu",
                            "CidrIp": "172.19.53.33/32"
                        }
                    ],
                    "ToPort": 22,
                    "IpProtocol": "tcp",
                    "UserIdGroupPairs": [],
                    "Ipv6Ranges": []
                }
            ],
            "GroupName": "IrvingSG",
            "VpcId": "vpc-018683f63819e6c4e",
            "OwnerId": "561448979833",
            "GroupId": "sg-08830c74ceb3f2a82"
        }
    ]
}
```

## Terraform templates:
- variables.tf - variables, no sensitive information, can be stored in Git
- aws_ami.tf - to find the proper Ubuntu 14.04 AMI in the region
- main.tf - creates an instance and Elastic IP, ouptupts the piblic IP

## Running the Terrafrom and connecting to the EC2 instance
The Terraform template outputs the assigned Elastic IP

```
glebtulukin@DESKTOP-1BJSHAN:/mnt/c/work/sandbox/07-23-pasadena/pasadena$ terraform apply
aws_eip.pasadena: Refreshing state... (ID: eipalloc-06e679d03c91cda6d)
data.aws_ami.ubuntu: Refreshing state...
aws_instance.my-pasadena-instance: Refreshing state... (ID: i-01376c8e043f09823)
aws_eip_association.eip_assoc: Refreshing state... (ID: eipassoc-030736af2958b1163)

Apply complete! Resources: 0 added, 0 changed, 0 destroyed.

Outputs:

instance_ip_addr = 3.219.228.206
glebtulukin@DESKTOP-1BJSHAN:/mnt/c/work/sandbox/07-23-pasadena/pasadena$ ssh -i ~/pasadena.pem ubuntu@3.219.228.206
```

## Scripts to run on a newly created box
```
sudo apt-get update #update
sudo apt-get install -y git ruby #install ruby and git
git clone https://gitlab.com/glebtulukin/pasadena.git #clone this repo
cd pasadena/
chmod 755 generate_html.rb #make it executable
export MY_API_KEY=xxxxxxx #otherwise the ruby script will exit with 1
./generate_html.rb
sudo apt install nginx
```
TODOs:
- add a script to create a cron job

## Check that the server running and accessible
Open the IP in your browser, you should see the default Nginx browser. 
http://3.219.228.206/



## Cron Job to run every 6 hours (not implemented)
The cron expression is `0 */6 * * *`

The job should run the Ruby script above and copy html to configured Nginx directory. 

## Nginx config (not implemented)

## User management (not implemented)
TODOs
- disable the ubuntu user
- create qless.admin with root privs
- create qless.user with Nginx permissions
