provider "aws" {
        region     = "${var.region}"
}

resource "aws_instance" "my-pasadena-instance" {
  ami             = "${data.aws_ami.ubuntu.id}"
  instance_type   = "t2.micro"
  key_name        = "pasadena"
  subnet_id       = "${var.subnet_id}"
  vpc_security_group_ids = ["${var.security_group_id}"]

  tags {
    Name = "The Pasadena EC2 Instance in the Irving VPC"
  }
}


resource "aws_eip_association" "eip_assoc" {
  instance_id   = "${aws_instance.my-pasadena-instance.id}"
  allocation_id = "${aws_eip.pasadena.id}"
}

resource "aws_eip" "pasadena" {
  vpc = true
}

output "instance_ip_addr" {
  value         = "${aws_eip_association.eip_assoc.public_ip}"
  description   = "The assigned Elastic IP"
}
